function foo() {
  bar();
}

function bar(a) {
  foo(a, a, a);
}

function baz(a) {
  foo(a);
}
